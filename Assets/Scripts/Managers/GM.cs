﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GM : Singleton<GM>
{
	#region Visible Fields
	[Header("Menus"),SerializeField]
	GameObject pauseMenu;
	[SerializeField] Transform shop;
	[SerializeField] GameObject menu;

	[Space, Header("Text"),SerializeField]
	Text speedText;
	[SerializeField] Text coinsText;
	[SerializeField] Text bestScoreText;

	[Space, Header("Counters Texts"), SerializeField]
	Text deathsText;
	[SerializeField] Text driftsText;
	[SerializeField] Text trailsReleasedText;
	[SerializeField] Text tapEffectsReleasedText;

	[Space, Header("Transforms")]
	[SerializeField] Transform optionsButton;
	[SerializeField] Transform optionsButtonHolder;

	[Space, Header("Other UI")]
	[SerializeField] Button pauseButton;
	[SerializeField] CanvasGroup preview;
	[SerializeField] GameObject noFundsPanelBackground;

	[Space, SerializeField]
	Vector2 carStartPos;
	#endregion

	#region Private Fields
	bool canTapToPlay = true;
	bool optionsExpanded = false;
	bool shopVisible = false;

	int[] shopPrices = { 5, 5, 10, 35 };

	GameState state = GameState.Menu;
	public bool IsPlaying()
	{
		return state == GameState.Playing;
	}
	public bool IsInMenu()
	{
		return this.state == GameState.Menu;
	}

	public event System.Action OnStartedPlaying = delegate { };
	#endregion

	#region Initialization
	private void Start()
	{
		Car.OnLost += Lose;
		InitGame();
		InitShop();
		SetUIValues(increaseTextValue: false);
	}

	void InitGame()
	{
		SetCar(SaveManager.Instance.data.activeCarIndex);

		//Sets the text for speedbar
		speedText.text = GetSpeedByIndex(SaveManager.Instance.data.activeSpeedIndex).ToString() + " MPH";
		bestScoreText.text = SaveManager.Instance.data.bestScores[SaveManager.Instance.data.activeSpeedIndex].ToString();
	}

	void InitShop()
	{
		//Initializing closer buttons and actual buttons
		//that will select specific cars (hidden buttons)
		int i = 0, j = 0;
		foreach (Transform cell in shop)
		{
			int _i = i, _j = j;

			//Closer Buttons
			Transform closer = cell.Find("Closer");

			if (closer != null)
			{
				closer.GetComponent<Button>().onClick.AddListener(() => OnCloserClick(closer.gameObject, _i));
				i++;
			}
			
			//Actual Buttons that select car
			if (cell.CompareTag("Cell"))
			{
				cell.GetComponent<Button>().onClick.AddListener(() => SetCar(_j));
				j++;
			}
		}
	}
	#endregion

	private void Update()
	{
		if (Input.GetMouseButtonUp(0) && IsInMenu() && !Helper.IsPointerOverUIObject() && canTapToPlay)
			StartPlaying();
	}

	#region Public Methods
	public void ResumeGame()
	{
		this.state = GameState.Playing;

		pauseMenu.SetActive(false);
		Time.timeScale = 1;

		StartCoroutine(Delay(0.1f));
	}

	public void PauseGame()
	{
		this.state = GameState.PauseMenu;

		pauseMenu.SetActive(true);
		Time.timeScale = 0;
	}

	public void OnOptionsClick()
	{
		ToggleOptions(!optionsExpanded);
	}

	public void OnSoundClick()
	{
		SaveManager.Instance.data.soundIndex = (SaveManager.Instance.data.soundIndex + 1) % 3;

		//Maybe Observer pattern on SoundManager
		if (SaveManager.Instance.data.soundIndex == 2)
			SoundManager.Instance.ToggleMusic(true);

		else SoundManager.Instance.ToggleMusic(false);
	}

	public void OnVibrationClick()
	{
		SaveManager.Instance.data.vibrationOn = !SaveManager.Instance.data.vibrationOn;

		if (SaveManager.Instance.data.vibrationOn)
			Handheld.Vibrate();
	}

	public void OnRateClick()
	{

	}

	public void OnTutorialClick()
	{

	}

	public void OnSpeedBarClick()
	{
		if (SaveManager.Instance.data.activeSpeedIndex >= SaveManager.Instance.data.unlockedSpeeds.Length)
			SaveManager.Instance.data.activeSpeedIndex = 0;

		else SaveManager.Instance.data.activeSpeedIndex = SaveManager.Instance.data.unlockedSpeeds
				[SaveManager.Instance.data.activeSpeedIndex] ? ++SaveManager.Instance.data.activeSpeedIndex : 0;

		speedText.text = GetSpeedByIndex(SaveManager.Instance.data.activeSpeedIndex).ToString() + " MPH";
		bestScoreText.text = SaveManager.Instance.data.bestScores[SaveManager.Instance.data.activeSpeedIndex].ToString();
		SaveManager.Instance.data.activeSpeedIndex = SaveManager.Instance.data.activeSpeedIndex;

		FindObjectOfType<TrailManager>().SetTapEffect(SaveManager.Instance.data.activeSpeedIndex);
	}

	public void OnShopClick()
	{
		noFundsPanelBackground.SetActive(false);
		shopVisible = !shopVisible;
		this.shop.gameObject.SetActive(shopVisible);

		if (!shopVisible)
			SetUIValues(false);

		else
		{
			int i = 0;
			foreach(Transform cell in shop)
			{
				Transform closer = cell.Find("Closer");

				if (closer == null)
					continue;

				if (SaveManager.Instance.data.unlockedCars[i] == true)
					closer.gameObject.SetActive(false);
			}
		}

		this.state = shopVisible ? GameState.Shop : GameState.Menu;
	}

	public void OnNoFundsClick()
	{
		noFundsPanelBackground.SetActive(false);
	}
	#endregion

	#region Private Methods
	void Lose()
	{
		this.state = GameState.Lost;

		AnimatePreview();
		SoundManager.Instance.PlaySound("Crash");
		StartCoroutine(Delay(2f));
		
		FindObjectOfType<CameraShake>().Shake(shakeAmount: (SaveManager.Instance.data.activeSpeedIndex) * .2f + .2f);
		if (SaveManager.Instance.data.vibrationOn)
			Handheld.Vibrate();

		if (CheckScore())
			SaveManager.Instance.data.coins += SaveManager.Instance.data.activeSpeedIndex + 1;

		SaveManager.Instance.data.playData += FindObjectOfType<Car>().playData;
		//Resetting PlayData
		FindObjectOfType<Car>().playData = new PlayData();
		SetUIValues(increaseTextValue: true);

		Car car = FindObjectOfType<Car>();
		car.transform.position = carStartPos;
		car.transform.rotation = Quaternion.identity;

		menu.SetActive(true);

		pauseButton.interactable = false;
		pauseButton.GetComponent<Animator>().SetTrigger("Fade Out");
	}

	void OnCloserClick(GameObject closer, int id)
	{
		int cost = shopPrices[id];

		if (SaveManager.Instance.data.coins >= cost)
		{
			SaveManager.Instance.UnlockCar(id);
			closer.SetActive(false);
			SetCar(id + 1);

			SaveManager.Instance.data.coins -= cost;
		}

		else noFundsPanelBackground.SetActive(true);
	}

	bool CheckScore()
	{
		bool res = ScoreManager.ActualScore > SaveManager.Instance.data.bestScores[SaveManager.Instance.data.activeSpeedIndex];
		if (res)
		{
			SaveManager.Instance.data.bestScores[SaveManager.Instance.data.activeSpeedIndex] = ScoreManager.ActualScore;
			this.bestScoreText.text = ScoreManager.ActualScore.ToString();
		}
		return res;
	}

	void SetCar(int newCar_Id)
	{
		if(FindObjectOfType<Car>() != null)
			Destroy(FindObjectOfType<Car>().gameObject);

		Instantiate(Resources.Load(@"Prefabs/Cars/Car_" + (newCar_Id).ToString()) as GameObject, carStartPos,
			Quaternion.identity, GameObject.Find("Environment").transform);

		SaveManager.Instance.data.activeCarIndex = newCar_Id;
	}

	void SetUIValues(bool increaseTextValue)
	{
		coinsText.text = SaveManager.Instance.data.coins.ToString();

		string deaths = SaveManager.Instance.data.playData.Deaths.ToString();
		string drifts = SaveManager.Instance.data.playData.drifts.ToString();
		string trailsReleased = SaveManager.Instance.data.playData.trailsReleased.ToString();
		string tapEffectsReleased = SaveManager.Instance.data.playData.tapEffectsReleased.ToString();

		if (!increaseTextValue)
		{
			deathsText.text = deaths;
			driftsText.text = drifts;
			trailsReleasedText.text = trailsReleased;
			tapEffectsReleasedText.text = tapEffectsReleased;
		}

		else
		{
			IncreaseTextValue(deathsText, deaths);
			IncreaseTextValue(driftsText, drifts);
			IncreaseTextValue(trailsReleasedText, trailsReleased);
			IncreaseTextValue(tapEffectsReleasedText, tapEffectsReleased);
		}
	}

	void StartPlaying()
	{
		pauseButton.GetComponent<Animator>().SetTrigger("Fade In");
		pauseButton.interactable = true;
		optionsExpanded = false;

		this.state = GameState.Playing;
		FindObjectOfType<Car>().Go(GetSpeedByIndex(SaveManager.Instance.data.activeSpeedIndex));

		menu.SetActive(false);

		OnStartedPlaying();
	}

	void ToggleOptions(bool display)
	{
		Animator[] animators = optionsButtonHolder.GetComponentsInChildren<Animator>();
		Button[] buttons = optionsButtonHolder.GetComponentsInChildren<Button>();

		float delay = 0;
		float step = display ? 0.07f : 0.02f;
		string trigger = display ? "Fade In" : "Fade Out";

		//Usual for in case of display == true, reversed for if display == false
		for (int i = display == true ? 0 : animators.Length - 1; display == true ? i <= animators.Length - 1 : i >= 0;
			i += display ? 1 : -1)
		{
			buttons[i].interactable = display ? true : false;

			StartCoroutine(PlayAnimDelayed(delay, animators[i], trigger));

			delay += step;
		}

		optionsExpanded = !optionsExpanded;
	}

	void AnimatePreview()
	{
		preview.GetComponent<Animator>().SetTrigger("Preview");
	}

	int GetSpeedByIndex(int index)
	{
		const int INITIAL_SPEED = 90, STEP = 20;
		return INITIAL_SPEED + STEP * index;
	}
	#endregion

	#region Coroutines
	IEnumerator PlayAnimDelayed(float delay, Animator animator, string trigger)
	{
		yield return new WaitForSeconds(delay);
		animator.SetTrigger(trigger);
	}

	IEnumerator Delay(float delay)
	{
		canTapToPlay = false;
		yield return new WaitForSeconds(delay);
		canTapToPlay = true;
		SetUIValues(true);
		state = GameState.Menu;
	}

	IEnumerator _increaseTextValue(Text toIncrease, int endValue)
	{
		int currentValue = int.Parse(toIncrease.text);
		float delay = 0.02f;

		while (currentValue != endValue)
		{
			yield return new WaitForSeconds(delay);
			currentValue++;
			toIncrease.text = currentValue.ToString();
		}
	}

	void IncreaseTextValue(Text toIncrease, string endValue) { IncreaseTextValue(toIncrease, int.Parse(endValue)); }

	void IncreaseTextValue(Text toIncrease, int endValue) { StartCoroutine(_increaseTextValue(toIncrease, endValue)); }
	#endregion
}