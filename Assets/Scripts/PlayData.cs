﻿public struct PlayData
{
	private int _deaths;
	public int drifts;
	public int trailsReleased;
	public int tapEffectsReleased;

	public int Deaths { get { return _deaths; } }

	public static PlayData operator +(PlayData p, PlayData other)
	{
		return new PlayData
		{
			drifts = p.drifts + other.drifts,
			trailsReleased = p.trailsReleased + other.trailsReleased,
			tapEffectsReleased = p.tapEffectsReleased + other.tapEffectsReleased,
			_deaths = ++p._deaths
		};
	}
}
