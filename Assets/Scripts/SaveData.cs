﻿public class SaveData
{
	public PlayData playData;

	public int coins = 0;
	public int[] bestScores = new int[4];
	public int activeCarIndex = 0;
	public int activeSpeedIndex = 0;

	// 110 and 130 and 150 MPH
	public bool[] unlockedSpeeds = new bool[3];
	public bool[] unlockedCars = new bool[4];

	//0 - No Sound, 1 - Sound Effects Only, 2 - Music and Sound Effects
	public int soundIndex = 2;
	public bool vibrationOn = true;
}
