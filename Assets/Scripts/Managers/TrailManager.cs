﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Car))]
public class TrailManager : MonoBehaviour 
{
	#region Variables
	[Header("Time")]
	[SerializeField] float timePerTrail = 0.2f;

	[SerializeField, Space] GameObject trailPrefab;
	[SerializeField] GameObject tapEffect;
	[SerializeField, Range(5, 60)] int trailCount = 10;

	Queue<Trail> trails = new Queue<Trail>();
	List<Trail> activeTrails = new List<Trail>();

	float lastTimeTrailAdded;
	bool driftCounterHelper = false;
	#endregion

	private void Start()
	{
		InitTrails();
	}

	private void Update()
	{
		if (NeedsTrail())
			LeaveTrail();

		if (NeedsTapEffect())
			CreateTapEffect();
	}

	void InitTrails()
	{
		for (int i = 0; i < trailCount; i++)
		{
			Trail trail = InitTrail();

			trails.Enqueue(trail);
		}
	}

	Trail InitTrail()
	{
		var trail = Instantiate(trailPrefab, TrailHolder);
		Trail trailComponent = trail.GetComponent<Trail>();

		return trailComponent;
	}

	Transform TrailHolder
	{
		get
		{
			return (GameObject.Find("Trail Holder") ?? new GameObject("Trail Holder")).transform;
		}
	}

	Transform TapEffectsHolder
	{
		get
		{
			return (GameObject.Find("Tap Effects Holder") ?? new GameObject("Tap Effects Holder")).transform;
		}
	}

	bool NeedsTrail()
	{
		if (GetComponent<Car>().IsDrifting)
		{
			if (driftCounterHelper == false)
			{
				GetComponent<Car>().playData.drifts++;
				driftCounterHelper = true;

				//This one shouldn't be responsible for doing it i reckon
				if(SaveManager.Instance.data.soundIndex == 1 || SaveManager.Instance.data.soundIndex == 2)
					SoundManager.Instance.PlaySound("Tire Screech");
			}

			lastTimeTrailAdded += Time.deltaTime;

			if (lastTimeTrailAdded > timePerTrail)
			{
				lastTimeTrailAdded = 0;
				return true;
			}
		}

		else driftCounterHelper = false;

		return false;
	}

	bool NeedsTapEffect()
	{
		if (GetComponent<Car>().IsMoving && Input.GetMouseButtonDown(0))
			return true;

		return false;
	}

	void CreateTapEffect()
	{
		Vector2 carPos = GetComponent<Car>().transform.position;

		Instantiate(tapEffect, carPos, Quaternion.identity, TapEffectsHolder);

		GetComponent<Car>().playData.tapEffectsReleased++;
	}

	void LeaveTrail()
	{
		Transform car = GetComponent<Car>().transform;
		Vector2 vel = GetComponent<Rigidbody2D>().velocity;

		Trail trail = trails.Dequeue();

		trail.transform.position = car.position;
		trail.transform.rotation = car.rotation;
		
		//Toggle animation
		trail.Show();

		trails.Enqueue(trail);
		activeTrails.Add(trail);

		GetComponent<Car>().playData.trailsReleased++;
	}

	public void SetTapEffect(int speedIndex)
	{
		tapEffect = Resources.Load(@"Prefabs/Tap Effects/Tap_Effect_" + (speedIndex + 1).ToString()) as GameObject;
	}
}
