﻿public enum GameState
{
	Menu,
	Playing,
	Lost,
	PauseMenu,
	Shop
}
