﻿using UnityEngine;

/// <summary>
/// Requires animator to have more than one state,
/// if animator has only one state this would not be triggered
/// </summary>
public class DestroyAfterAnimation : MonoBehaviour 
{
	string startingClipName;

	private void Start()
	{
		startingClipName = ClipName();
	}

	private void Update()
	{
		string currentClipName = ClipName();

		if (currentClipName != startingClipName)
			Destroy(gameObject);
	}

	string ClipName()
	{
		return GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.name;
	}
}
