﻿using UnityEngine;

public class Trail : MonoBehaviour 
{
	Animator animator;

	private void Start()
	{
		animator = GetComponent<Animator>();
	}

	public void Show()
	{
		animator.SetTrigger("Trail");
	}
}
