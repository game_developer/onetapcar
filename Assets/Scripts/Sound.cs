﻿using UnityEngine;

[System.Serializable]
public struct Sound
{
	public string name;
	public AudioClip clip;
	public bool loop;
	public bool isSoundEffect;
	[Range(0, 1)] public float volume;
	[Range(0, 3)] public float pitch;
}

public struct Audio
{
	public AudioSource source;
	public bool isSoundEffect;

	public Audio(AudioSource source, bool isSoundEffect)
	{
		this.source = source;
		this.isSoundEffect = isSoundEffect;
	}
}
