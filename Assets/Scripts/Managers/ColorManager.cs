﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColorManager : MonoBehaviour 
{
	[SerializeField] Color[] colors;
	[SerializeField] float transitionDuration;
	[SerializeField] float startPlayingDuration;

	private ColorItem[] GetColorItems()
	{
		return FindObjectsOfType<ColorItem>();
	}

	Color lastColor;

	Color GetRandomColor()
	{
		List<Color> possibleColors = colors.ToList();
		Color result;

		if(lastColor != default(Color))
		{
			possibleColors.Remove(lastColor);
		}

		result = possibleColors[Random.Range(0, possibleColors.Count - 1)]; ;

		lastColor = result;
		return result;
	}

	private void Start()
	{
		SetColorInstantly();

		Car.OnLost += SetUIColorsInstantly;
		GM.Instance.OnStartedPlaying += ChangeColorFaster;
		FindObjectOfType<ScoreManager>().OnScoreIncreased += ChangeColorSlower;
	}

	void ChangeColorFaster()
	{
		ChangeColorSmoothly(startPlayingDuration);
	}

	void ChangeColorSlower()
	{
		ChangeColorSmoothly(transitionDuration);
	}

	void ChangeColorSmoothly(float duration)
	{
		Color rndColor = GetRandomColor();

		foreach (ColorItem cItem in GetColorItems())
		{
			cItem.ChangeColorSmoothly(rndColor, duration);
		}
	}

	void SetColorInstantly()
	{
		Color rndColor = GetRandomColor();

		foreach (ColorItem cItem in GetColorItems())
			cItem.Color = rndColor;																							
	}

	/// <summary>
	/// This is for setting the colors of options
	/// </summary>
	void SetUIColorsInstantly()
	{
		ColorItem[] colorItems = Resources.FindObjectsOfTypeAll<ColorItem>();

		foreach (ColorItem cItem in colorItems)
			cItem.Color = lastColor;
	}
}
