﻿using UnityEngine;
using System;

public class SaveManager : Singleton<SaveManager> 
{
	public SaveData data;
	const string SAVE = "Save";

	#region Save
	private void Awake()
	{
		Load();
	}

#if	UNITY_ANDROID && !UNITY_EDITOR
	private void OnApplicationPause()
	{
		Save();
	}
#endif
	private void OnApplicationQuit()
	{
		Save();
	}

	void Load()
	{
		if (!PlayerPrefs.HasKey(SAVE))
			data = new SaveData();

		else
			data = Helper.Deserialize<SaveData>(Helper.Decrypt(PlayerPrefs.GetString(SAVE)));
	}

	void Save()
	{
		PlayerPrefs.SetString(SAVE, Helper.Encrypt(Helper.Serialize(data)));
	}

	void ResetSave()
	{
		PlayerPrefs.DeleteKey(SAVE);
	}
	#endregion

	public void UnlockCar(int id)
	{
		data.unlockedCars[id] = true;

		UnlockSpeeds(id);
	}

	void UnlockSpeeds(int carId)
	{
		switch (carId)
		{
			case 0:
			case 1:
				this.data.unlockedSpeeds[0] = true;
				break;

			case 2:
				for (int i = 0; i < 2; i++)
				{
					this.data.unlockedSpeeds[i] = true;
				}
				break;
			case 3: 
				for(int i = 0; i < 3; i++)
				{
					this.data.unlockedSpeeds[i] = true;
				}
				break;
			default: throw new NotImplementedException();
		}
	}
}
