﻿using System.Collections;
using UnityEngine;

public class FinishLine : MonoBehaviour 
{
	[SerializeField] float moveDuration;
	[SerializeField] AnimationCurve curve;

	bool isRight = true;

	public event System.Action<bool> OnFinishCrossed = delegate { };

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Player"))
		{
			Switch();
		}
	}

	void Switch()
	{
		OnFinishCrossed(isRight ? true : false);

		this.transform.localScale = Vector3.zero;

		SwitchPos();

		StartCoroutine(Display());
	}

	void SwitchPos()
	{
		Vector3 newPos = this.transform.position;
		newPos.x = -newPos.x;
		this.transform.position = newPos;

		isRight = !isRight;
	}

	IEnumerator Display()
	{
		float curTime = 0;
		float xScale = 0;

		while (curTime <= moveDuration)
		{
			curTime += Time.deltaTime;

			xScale = curve.Evaluate(curTime / moveDuration);
			if (!isRight)
				xScale *= -1;
			this.transform.localScale = new Vector3(xScale, 1, 1);

			yield return null;
		}
	}
}
