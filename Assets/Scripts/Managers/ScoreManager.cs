﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
	[SerializeField] Text scoreText;
	[SerializeField] Text actualScoreText;
	[SerializeField] Animator gfxAnim;

	int score = 1;
	static int actualScore = 0;
	int scoreCounter = 0;
	const float delayTime = 0.1f;

	public static int ActualScore { get { return actualScore; } }

	public event System.Action OnScoreIncreased = delegate { };

	private void Start()
	{
		FindObjectOfType<FinishLine>().OnFinishCrossed += ChangeScore;
		GM.Instance.OnStartedPlaying += ResetScore;
	}

	void ResetScore()
	{
		score = 1;
		actualScore = 0;
		scoreCounter = 0;

		scoreText.text = 1.ToString();
		actualScoreText.text = 0.ToString();
	}

	void ChangeScore(bool temp)
	{
		actualScore += score;
		scoreCounter++;

		actualScoreText.text = actualScore.ToString();

		if (scoreCounter == score)
			StartCoroutine(IncreaseScore());
	}

	IEnumerator IncreaseScore()
	{
		scoreCounter = 0;
		score++;
		gfxAnim.SetTrigger("Increase");

		yield return new WaitForSeconds(delayTime);

		scoreText.text = score.ToString();

		OnScoreIncreased();
	}
}
