﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(TrailManager))]
public class Car : MonoBehaviour
{
	#region Variables
	[HideInInspector] public PlayData playData;
	//How much car is going to turn compared to movespeed
	[SerializeField] float turnMultiplier;

	public bool IsDrifting { get { return turnTime > 0.2f; } }
	public bool IsMoving
	{
		get { return _isMoving; }
		set
		{
			_isMoving = value;
			acceptInput = false; turn = false; turnTime = 0;
		}
	}
	private bool _isMoving;
	bool acceptInput;
	bool turn;
	float turnTime;
	float moveSpeed;
	float turnSpeed;

	public static event System.Action OnLost = delegate { };
	#endregion

	void Update()
	{ 
		if (acceptInput && !Helper.IsPointerOverUIObject())
			HandleInput();
	}

	void FixedUpdate()
	{
		if (IsMoving)
			HandleMovement();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Map"))	
		{
			OnLost();
			Stop();
		}
	}

	void HandleInput()
	{
		if (Input.GetMouseButton(0))
		{
			turn = true;
			turnTime += Time.deltaTime;
		}

		else
		{
			turn = false;
			turnTime = 0;
		}
	}

	void HandleMovement()
	{
		Rigidbody2D rb = GetComponent<Rigidbody2D>();
		transform.Translate(Vector2.up * moveSpeed * Time.fixedDeltaTime, Space.Self);

		if (turn)
		{
			if (turnTime < 0.1)
				rb.MoveRotation(rb.rotation - 1.8f * turnMultiplier);
			else
				rb.MoveRotation(rb.rotation - turnMultiplier);
		}
	}

	public void Go(float moveSpeed)
	{
		this.moveSpeed = moveSpeed / 30;
		this.turnSpeed = this.moveSpeed * turnMultiplier;
		IsMoving = true;

		StartCoroutine(DelayInput());
	}

	IEnumerator DelayInput()
	{
		yield return new WaitForSeconds(0.1f);
		acceptInput = true;
	}

	void Stop()
	{
		IsMoving = false;
	}
}