﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Applies to both Image and SpriteRenderer components
/// </summary>
public class ColorItem : MonoBehaviour
{
	public Color Color
	{
		get
		{
			SpriteRenderer spR = GetComponent<SpriteRenderer>();
			return spR != null ? spR.color : this.GetComponent<Image>().color;
		}

		set
		{
			SpriteRenderer spR = GetComponent<SpriteRenderer>();

			if (spR != null)
				spR.color = value;

			else
				this.GetComponent<Image>().color = value;
		}
	}

	public void ChangeColorSmoothly(Color color, float duration)
	{
		StartCoroutine(_changeColorSmoothly(color, duration));
	}

	private IEnumerator _changeColorSmoothly(Color newColor, float duration)
	{
		Color prevColor = Color;
		float curTime = 0;

		while(curTime < duration)
		{
			curTime += Time.deltaTime;

			Color = Color.Lerp(prevColor, newColor, curTime / duration);

			yield return null;
		}
	}
}
