﻿using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager> 
{
	public Sound[] sounds;
	[HideInInspector] public Dictionary<string, Audio> audioSources = new Dictionary<string, Audio>();

	private void Awake()
	{
		for (int i = 0; i < sounds.Length; i++)
		{
			Sound curSound = sounds[i];

			AudioSource audioSource = gameObject.AddComponent<AudioSource>();
			audioSource.clip = curSound.clip;
			audioSource.loop = curSound.loop;
			audioSource.pitch = curSound.pitch;
			audioSource.volume = curSound.volume;

			audioSources.Add(curSound.name, new Audio(audioSource, curSound.isSoundEffect));
		}
	}

	public void Start()
	{
		if (SaveManager.Instance.data.soundIndex == 2)
			ToggleMusic(true);
	}

	public void ToggleMusic(bool on)
	{
		if (on)
			audioSources["Music"].source.Play();
		else
			audioSources["Music"].source.Stop();
	}

	public void PlaySound(string soundName)
	{
		if (!audioSources.ContainsKey(soundName))
		{
			Debug.LogWarning("The sound with key " + soundName + " doesn't exist!");
			return;
		}

		if (audioSources[soundName].isSoundEffect && SaveManager.Instance.data.soundIndex == 0)
			return;

		audioSources[soundName].source.Play();
	}
}
